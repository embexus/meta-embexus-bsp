FILESEXTRAPATHS_prepend := "${THISDIR}/u-boot:"

UBOOT_ENV_beaglebone = "uEnv"

SRC_URI_append_beaglebone = " file://uEnv.txt \
                              file://Chipsee_LCD7_cape_enable_backlight.patch"
